﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace mlg_audiomngr {
    [System.Serializable]
    public enum AudioFileType {
        sfx,
        ost,
        env
    }

    [CreateAssetMenu (fileName = "audioFile", menuName = "ScriptableObjects/AudioFileSO", order = 1)]
    public class AudioFileSO : ScriptableObject {
        public string audioName;
        public AudioFileType type;

        public AudioClip audioClip;
        public AudioMixerGroup output;

        [Range (0f, 1f)]
        public float volume;

        [HideInInspector]
        public AudioSource source;

        public bool isLooping;

        public bool playOnAwake;
    }
}