﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Audio;

namespace mlg_audiomngr {
    public class AudioMngr : MonoBehaviour {
        //Variables
        public static AudioMngr instance;
        public AudioMixer mixer;

        //Put all the scriptable objects here
        public AudioFileSO[] sfx;
        public AudioFileSO[] env;
        public AudioFileSO[] ost;

        private void Awake () {
            if (instance == null) {
                instance = this;
                DontDestroyOnLoad (this);
            } else {
                Destroy (this.gameObject);
            }

            foreach (var s in sfx) {
                s.source = gameObject.AddComponent<AudioSource> ();
                s.source.clip = s.audioClip;
                s.source.volume = s.volume;
                s.source.loop = s.isLooping;
                s.source.outputAudioMixerGroup = s.output;
            }
            foreach (var s in ost) {
                s.source = gameObject.AddComponent<AudioSource> ();
                s.source.clip = s.audioClip;
                s.source.volume = s.volume;
                s.source.loop = s.isLooping;
                s.source.outputAudioMixerGroup = s.output;
            }
            foreach (var s in env) {
                s.source = gameObject.AddComponent<AudioSource> ();
                s.source.clip = s.audioClip;
                s.source.volume = s.volume;
                s.source.loop = s.isLooping;
                s.source.outputAudioMixerGroup = s.output;
            }

        }

        public AudioFileSO[] GetArray (AudioFileType type) {
            switch (type) {
                case AudioFileType.sfx:
                    return instance.sfx;

                case AudioFileType.ost:
                    return instance.ost;

                case AudioFileType.env:
                    return instance.env;

                default:
                    return null;
            }
        }

        public static void Play (string name, AudioFileType type) {
            AudioFileSO[] array = instance.GetArray (type);
            AudioFileSO s = Array.Find (array, AudioFileSO => AudioFileSO.audioName == name);

            if (s == null) {
                Debug.LogError ("Sound name: " + name + " not found!");
                return;
            } else {
                s.source.Play ();
            }
        }

        public static void Pause (string name, AudioFileType type) {
            AudioFileSO[] array = instance.GetArray (type);
            AudioFileSO s = Array.Find (array, AudioFileSO => AudioFileSO.audioName == name);

            if (s == null) {
                Debug.LogError ("Sound name: " + name + " not found!");
                return;
            } else {
                s.source.Pause ();
            }
        }

        public static void UnPause (string name, AudioFileType type) {
            AudioFileSO[] array = instance.GetArray (type);
            AudioFileSO s = Array.Find (array, AudioFileSO => AudioFileSO.audioName == name);

            if (s == null) {
                Debug.LogError ("Sound name: " + name + " not found!");
                return;
            } else {
                s.source.UnPause ();
            }
        }

        public static void Stop (string name, AudioFileType type) {
            AudioFileSO[] array = instance.GetArray (type);
            AudioFileSO s = Array.Find (array, AudioFileSO => AudioFileSO.audioName == name);

            if (s == null) {
                Debug.LogError ("Sound name: " + name + " not found!");
                return;
            } else {
                s.source.Stop ();
            }
        }

        //5.FadeIn
        //6.FadeOut
        //7.lower volume for a duration
    }
}