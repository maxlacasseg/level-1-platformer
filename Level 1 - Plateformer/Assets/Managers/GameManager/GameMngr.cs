﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMngr : MonoBehaviour {
    public static GameMngr instance;

    public delegate void OnStateChangeHandler (GameState state);
    public event OnStateChangeHandler OnStateChange;

    public GameState state;

    //Define game constants
    //Define game variables

    void Awake () {
        if (instance != null) {
            Destroy (this);
        } else {
            instance = this;
        }

    }

    public void SwitchState (GameState state) {
        instance.state = state;
        if (OnStateChange != null) {
            OnStateChange (state);
        }
    }
}